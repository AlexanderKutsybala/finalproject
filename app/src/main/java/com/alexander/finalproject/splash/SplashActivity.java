package com.alexander.finalproject.splash;

import android.content.*;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.alexander.finalproject.R;
import com.alexander.finalproject.Repository;
import com.alexander.finalproject.login.LoginActivity;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.MainScreenActivity;
import com.alexander.finalproject.mainscreen.profile.data.MainUser;

import java.util.ArrayList;
import java.util.function.LongFunction;

public class SplashActivity extends AppCompatActivity implements ISplashContract.ISplashView {


    private BroadcastReceiver broadCastReciever;

    private String myPreferences = "myPrefs";

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            sharedPreferences = getSharedPreferences(myPreferences, MODE_PRIVATE);
            String str = sharedPreferences.getString("cookie1", "0");
            RegistrationResponse.token=str.substring(str.indexOf("=")+1, str.indexOf(";"));



            RegistrationResponse.cookie = sharedPreferences.getString("cookie2","0")+"; "
                    +sharedPreferences.getString("cookie1","0");
        }

        catch (Exception e){
            onErrorAuth();
        }



        SplashPresenter presenter = new SplashPresenter(this);



        broadCastReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager != null) {
                    if (connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected()) {
                        presenter.isRegister(RegistrationResponse.cookie,Repository.getInstance());
                    } else {
                        onNoInternet();
                    }
                }
            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadCastReciever, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadCastReciever);
    }

    @Override
    public void onOkAuth() {


        String str = sharedPreferences.getString("cookie1", "0");
        RegistrationResponse.token=str.substring(str.indexOf("=")+1, str.indexOf(";"));

        RegistrationResponse.cookie=(sharedPreferences.getString("cookie2","0"))+"; "+
        (sharedPreferences.getString("cookie1","0"));

        startActivity(new Intent(this, MainScreenActivity.class));
        finish();

    }

    @Override
    public void onErrorAuth() {

        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    @Override
    public void onNoInternet() {
        Toast.makeText(this,R.string.no_internet,Toast.LENGTH_SHORT).show();
    }

}
