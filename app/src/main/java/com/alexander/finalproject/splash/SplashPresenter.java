package com.alexander.finalproject.splash;

import android.util.Log;
import com.alexander.finalproject.AppDataBase;
import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.mainscreen.profile.UserDataBase;
import com.alexander.finalproject.mainscreen.profile.data.MainUser;
import com.alexander.finalproject.mainscreen.profile.data.User;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

public class SplashPresenter implements ISplashContract.ISplashPresenter {


    private ISplashContract.ISplashView listener;
    private UserDataBase database;

    public SplashPresenter(ISplashContract.ISplashView listener){
        this.listener = listener;
    }



    @Override
    public void isRegister(String cookie, IRepository repository) {

        if (cookie==null)
            listener.onErrorAuth();

        else{

            database = AppDataBase.getInstance().getDatabase();

            repository.initRetrofit()
                    .isRegisterUser(cookie)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            response -> {
                                Single.just(response)
                                        .subscribeOn(Schedulers.io())
                                        .subscribe(v->{
                                            User dataBaseUser = new User();
                                            dataBaseUser.setUd(0);
                                            dataBaseUser.setIdE(response.getUser().getIdE());
                                            dataBaseUser.setAdmin(response.getUser().getAdmin());
                                            dataBaseUser.setClient(response.getUser().getClient());
                                            dataBaseUser.setDepartment(response.getUser().getDepartment());
                                            dataBaseUser.setDescription(response.getUser().getDescription());
                                            dataBaseUser.setFaculty(response.getUser().getFaculty());
                                            dataBaseUser.setGrade(response.getUser().getGrade());
                                            dataBaseUser.setGraduate(response.getUser().getGraduate());
                                            dataBaseUser.setWork(response.getUser().getWork());
                                            dataBaseUser.setUd(response.getUser().getUd());
                                            dataBaseUser.setSkype(response.getUser().getSkype());
                                            dataBaseUser.setSize(response.getUser().getSize());
                                            dataBaseUser.setSchool(response.getUser().getSchool());
                                            dataBaseUser.setResume(response.getUser().getResume());
                                            dataBaseUser.setPhone(response.getUser().getPhone());
                                            dataBaseUser.setNotifications(response.getUser().getNotifications());
                                            dataBaseUser.setGraduateUn(response.getUser().getGraduateUn());
                                            dataBaseUser.setName(response.getUser().getName());
                                            dataBaseUser.setLastName(response.getUser().getLastName());
                                            dataBaseUser.setMiddleName(response.getUser().getMiddleName());
                                            dataBaseUser.setAvatar(response.getUser().getAvatar());
                                            dataBaseUser.setRegion(response.getUser().getRegion());
                                            dataBaseUser.setUniversity(response.getUser().getUniversity());
                                            dataBaseUser.setEmail(response.getUser().getEmail());
                                            dataBaseUser.setBirthday(response.getUser().getBirthday());

                                            try{
                                                database.userDao().insert(dataBaseUser);}
                                            catch (Exception e){
                                                database.userDao().update(dataBaseUser);
                                            }
                                        });
                            onAuth(response);}, error->{
                                listener.onNoInternet();});


        }

    }

    public void onAuth(MainUser user) {

        if (user.getStatus().equals("Error"))
            listener.onErrorAuth();
        if (user.getStatus().equals("Ok"))
            listener.onOkAuth();
    }

}
