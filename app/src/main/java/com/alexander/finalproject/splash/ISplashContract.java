package com.alexander.finalproject.splash;

import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.mainscreen.profile.data.MainUser;

import java.util.List;

public interface ISplashContract {

    interface ISplashView{

        void onOkAuth();
        void onErrorAuth();
        void onNoInternet();

    }

    interface ISplashPresenter{

        void isRegister(String cookie, IRepository repository);
    }
}
