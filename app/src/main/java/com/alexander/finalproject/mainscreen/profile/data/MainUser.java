package com.alexander.finalproject.mainscreen.profile.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainUser {

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("status")
    @Expose
    private String status;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
