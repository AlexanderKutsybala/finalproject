package com.alexander.finalproject.mainscreen

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import com.alexander.finalproject.R
import com.alexander.finalproject.mainscreen.homework.view.MyCourseFragment
import com.alexander.finalproject.mainscreen.profile.Profile
import com.alexander.finalproject.mainscreen.settings.Settings


class MainScreenActivity : AppCompatActivity() {

    private val tabIcons = intArrayOf(R.drawable.mycourse, R.drawable.profile, R.drawable.settings)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_screen)



        // Получаем ViewPager и устанавливаем в него адаптер
        val viewPager = findViewById<ViewPager>(R.id.viewpager)
        val adapter = MainScreenFragmentPageAdapter(supportFragmentManager)


        adapter.addFrag(MyCourseFragment(),getString(R.string.myСourse))
        adapter.addFrag(Profile(),getString(R.string.profile))
        adapter.addFrag(Settings(),getString(R.string.settings))
        viewPager.adapter = adapter
        viewPager.currentItem=1


        // Передаём ViewPager в TabLayout
        val tabLayout = findViewById<TabLayout>(R.id.sliding_tabs)
        tabLayout.setupWithViewPager(viewPager)

        setupTab(tabLayout)

    }

    private fun setupTab(tabLayout: TabLayout) {

        for (indexOfTab in 0 until tabIcons.size) {
            tabLayout.getTabAt(indexOfTab)?.setIcon(tabIcons[indexOfTab])
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        moveTaskToBack(true)
    }

}