package com.alexander.finalproject.mainscreen.homework.test;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alexander.finalproject.R;

import java.util.ArrayList;
import java.util.List;

public class TestFragment extends Fragment {


    int[] answer;

    private final static String QUESTION = "questions";
    private final static String CHOISE = "choise";

    private static Boolean choose;

    public static TestFragment newInstance(List<String> questions, boolean choise){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(questions);
        Bundle bun = new Bundle();
        bun.putStringArrayList(QUESTION,arrayList);
        bun.putBoolean(CHOISE,choise);

        TestFragment fragment = new TestFragment();
        fragment.setArguments(bun);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ArrayList<String> questions = getArguments().getStringArrayList(QUESTION);
        Boolean multiple = getArguments().getBoolean(CHOISE);
        Context context = getContext();

        choose=false;

        answer =new int[questions.size()];

        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        int schetchik=0;
        for (String question: questions){

            TextView text = new TextView(context);
            text.setText(question);

            if (schetchik==0)
                text.setTextSize(25);
            else {
                text.setTextSize(20);

                int finalSchetchik = schetchik;
                text.setOnClickListener(v -> {

                    if (!multiple){


                        if (text.getBackground() == null && !choose) {

                            text.setBackground(getResources().getDrawable(R.drawable.test_shape, null));
                            answer[finalSchetchik]=1;
                            choose = true;
                        } else if (text.getBackground()!=null) {

                            text.setBackground(null);
                            answer[finalSchetchik] = 0;
                            choose = false;
                        }

                    } else{
                        if (text.getBackground() == null ) {

                            text.setBackground(getResources().getDrawable(R.drawable.test_shape, null));
                            answer[finalSchetchik]=1;
                        } else {

                            text.setBackground(null);
                            answer[finalSchetchik]=0;
                        }
                    }
                });
            }

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(20, 20, 20, 20);

            layout.addView(text,params);
            schetchik++;
        }

        return layout;

    }


    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

    }
}
