package com.alexander.finalproject.mainscreen.homework.test.testdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Questions {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("position")
    @Expose
    public int position;

    @SerializedName("problem")
    @Expose
    public Problem problem;


    @SerializedName("status")
    @Expose
    public String problemStatus;



}
