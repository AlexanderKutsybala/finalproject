package com.alexander.finalproject.mainscreen.homework.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.alexander.finalproject.R;
import com.alexander.finalproject.Repository;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.homework.homeworkdata.MustTask;
import com.alexander.finalproject.mainscreen.homework.test.TestActivity;
import com.alexander.finalproject.mainscreen.homework.HomeWorkContract;
import com.alexander.finalproject.mainscreen.homework.HomeWorkPresenter;
import com.alexander.finalproject.mainscreen.homework.ReposytoryHomeWork;

import java.util.List;


public class MyCourseFragment extends Fragment implements HomeWorkContract.MyCourseFragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private RecyclerView mRecyclerView;
    public HomeWorkAdapter mListadapter;
    private HomeWorkPresenter homeWorkPresenter;
    private List<MustTask> mustTasks;
    private int index;

    private int indexOfElement=-1;


    private HomeWorkContract.MyCourseFragment myCourseFragment;

    public MyCourseFragment(){


    }

    public static MyCourseFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        MyCourseFragment fragment = new MyCourseFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.my_course, container, false);

        mRecyclerView =  view.findViewById(R.id.recyclerView);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mRecyclerView.setLayoutManager(layoutManager);
        homeWorkPresenter.getHomeWork(new ReposytoryHomeWork(homeWorkPresenter),RegistrationResponse.cookie);

        if (indexOfElement!=-1)
            mRecyclerView.smoothScrollToPosition(indexOfElement);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        homeWorkPresenter = new HomeWorkPresenter(this, Repository.getInstance());

    }

    @Override
    public void setTests(List<MustTask> mustTusks, int index) {

        this.mustTasks = mustTusks;
        this.index = index;

        mListadapter = new HomeWorkAdapter(mustTusks,t->homeWorkPresenter.startTest(RegistrationResponse.cookie, mustTusks.get(index).task.contest_info.contest_url),getContext());
        mRecyclerView.setAdapter(mListadapter);

        if (index>=0) {

            indexOfElement=index;
            mRecyclerView.scrollToPosition(index);
        }
    }

    @Override
    public void startTest() {

        TestActivity.start(this.getActivity(),mustTasks.get(index).task.title,
                mustTasks.get(index).task.contest_info.contest_url);
    }

    @Override
    public void onError() {
        Toast.makeText(getContext(),R.string.errorChange,Toast.LENGTH_SHORT).show();
    }
}
