package com.alexander.finalproject.mainscreen.settings;


public interface SettingsContract {

    interface ISettingsView{
        void showLoading();
        void showError();
        void exit();
    }
    interface ISettingsPresenter{
        void logOut(String cookie);
    }
}
