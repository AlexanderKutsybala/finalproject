package com.alexander.finalproject.mainscreen.homework.test;

import com.alexander.finalproject.mainscreen.homework.test.testdata.Questions;

import java.util.List;

public interface TestContract {

    interface TestActivityInterface{

        void showTest(List<Questions> questions);
        void onError();
        void refresh(int id);


    }

    interface TestPresenterInteface{

        void sendAnswer(int[] answer, int id, String testId);

    }
}
