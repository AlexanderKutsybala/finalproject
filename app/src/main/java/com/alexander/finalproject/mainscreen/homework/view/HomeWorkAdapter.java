package com.alexander.finalproject.mainscreen.homework.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import com.alexander.finalproject.R;
import com.alexander.finalproject.mainscreen.homework.homeworkdata.MustTask;

import java.util.List;



public class HomeWorkAdapter extends RecyclerView.Adapter<HomeWorkAdapter.MyViewHolder>  {

    private List<MustTask> mustTasks;
    private OnItemClickListener listener;
    private Context context;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Context context;

        private TextView titleLecture;
        private TextView titleTest;
        private TextView status;
        OnItemClickListener listener;


        public MyViewHolder(View v, OnItemClickListener listener) {
            super(v);

            context = v.getContext();

            this.listener = listener;
            titleLecture = v.findViewById(R.id.titleLecture);
            titleTest = v.findViewById(R.id.titleTest);
            status = v.findViewById(R.id.status);

        }


        void bind(MustTask mustTask) {

            titleLecture.setText(mustTask.name);
            titleTest.setText(mustTask.task.title);

           if (mustTask.task.contest_info.contest_status.status.equals("announcement")){
                status.setText("Скоро будет!");
                status.setTextColor(Color.parseColor("#FF0033"));
                titleTest.setPaintFlags(0);
                titleTest.setOnClickListener(null);

            }
            else if (mustTask.task.contest_info.contest_status.status.equals("ongoing")){
                status.setText("Тест Идет!");
                status.setTextColor(Color.parseColor("#FF9900"));
                titleTest.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
                titleTest.setOnClickListener(v -> listener.onClick(mustTask));

            }
            else if (mustTask.task.contest_info.contest_status.status.equals("contest_review")){
                status.setText("Тест прошел!");
                status.setTextColor(Color.parseColor("#00CC00"));
                titleTest.setPaintFlags(0);
                titleTest.setOnClickListener(null);
            }


        }
    }


    public HomeWorkAdapter(List<MustTask> mustTasks, OnItemClickListener clickListener, Context context) {
        this.mustTasks = mustTasks;
        this.listener = clickListener;
        this.context = context;

    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.test_item, parent, false);

        return new MyViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int i) {


        holder.bind(mustTasks.get(i));
/*
        Animation animation = AnimationUtils.loadAnimation(context,
                R.anim.up_from_bottom);
        holder.itemView.startAnimation(animation);*/

    }

    @Override
    public int getItemCount() {
        return mustTasks.size();
    }


    public List<MustTask> getData() {
        return mustTasks;
    }

    public void setData(List<MustTask> mustTasks) {
        this.mustTasks = mustTasks;
        notifyDataSetChanged();
    }


    public interface OnItemClickListener {
        void onClick(MustTask mustTask);
    }

}
