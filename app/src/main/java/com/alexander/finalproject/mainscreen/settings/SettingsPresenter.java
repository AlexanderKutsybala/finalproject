package com.alexander.finalproject.mainscreen.settings;
import android.util.Log;
import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.homework.test.testdata.Pust;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;



public class SettingsPresenter implements SettingsContract.ISettingsPresenter {

    SettingsContract.ISettingsView listener;
    IRepository repo;

    public SettingsPresenter(SettingsContract.ISettingsView listener, IRepository repo){
        this.listener = listener;
        this.repo = repo;
    }

    @Override
    public void logOut(String cookie) {

        listener.showLoading();
        repo.initRetrofit()
                .signOut(cookie,"https://fintech.tinkoff.ru/",RegistrationResponse.token,new Pust())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> listener.exit(), error -> {
                    listener.showError();});
    }
}
