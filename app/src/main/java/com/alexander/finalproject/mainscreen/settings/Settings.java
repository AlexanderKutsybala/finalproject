package com.alexander.finalproject.mainscreen.settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.alexander.finalproject.R;
import com.alexander.finalproject.Repository;
import com.alexander.finalproject.login.LoginActivity;
import com.alexander.finalproject.login.data.RegistrationResponse;


public class Settings extends Fragment implements SettingsContract.ISettingsView{
    public static final String ARG_PAGE = "ARG_PAGE";

    private Button buttonSignOut;
    private ProgressBar progressBar;

    private int mPage;

    public Settings(){

    }

    public static Settings newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        Settings fragment = new Settings();
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPage = getArguments().getInt(ARG_PAGE);
        }
    }

    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings, container, false);

        progressBar=view.findViewById(R.id.loadingBar);

        buttonSignOut=view.findViewById(R.id.buttonSignOut);
        buttonSignOut.setOnClickListener(v->{
            SettingsPresenter presenter = new SettingsPresenter(this,Repository.getInstance());
            presenter.logOut(RegistrationResponse.cookie);

        });

        return view;
    }

    @Override
    public void showLoading() {

        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void showError() {

        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(getContext(),getResources().getText(R.string.errorChange),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void exit() {
        progressBar.setVisibility(View.INVISIBLE);
        startActivity(new Intent(this.getActivity(),LoginActivity.class));
        this.getActivity().finish();
    }
}
