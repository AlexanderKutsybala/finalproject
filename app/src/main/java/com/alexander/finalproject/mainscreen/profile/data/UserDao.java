package com.alexander.finalproject.mainscreen.profile.data;

import android.arch.persistence.room.*;
import io.reactivex.Flowable;
import io.reactivex.Single;

import java.util.List;

@Dao
public interface UserDao {

    @Insert
    void insertAll(User... users);

    @Insert
    void insert(User user);

    @Delete
    void delete(User user);

    @Update
    void update(User user);

    @Query("SELECT * FROM user WHERE ud = :ud")
    Single<User> getById(int ud);

    @Query("SELECT * FROM user")
    List<User> getAllUsers();

}
