package com.alexander.finalproject.mainscreen.homework;

import com.alexander.finalproject.Api;
import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.IReposytoryHomeWork;
import com.alexander.finalproject.mainscreen.homework.homeworkdata.HomeWorks;
import com.alexander.finalproject.mainscreen.homework.homeworkdata.MustTask;
import com.alexander.finalproject.mainscreen.homework.homeworkdata.Tasks;
import com.alexander.finalproject.mainscreen.homework.test.testdata.Pust;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.List;

public class HomeWorkPresenter implements HomeWorkContract.HomeWorkPresenter {

    private static final String TASK_TYPE = "test_during_lecture";

    private static final String CONTEST_STATUS_FIRST = "announcement";
    private static final String CONTEST_STATUS_SECOND = "ongoing";
    private static final String CONTEST_STATUS_THIRD = "contest_review";

    private final String referer = "https://fintech.tinkoff.ru";

    private  ArrayList<MustTask> mustTasks;

    private HomeWorkContract.MyCourseFragment listener;

    private int index;

    private String testId;
    private String cookie;

    private Api api;

    private IRepository repository;

    public HomeWorkPresenter(HomeWorkContract.MyCourseFragment listener, IRepository repository){

        this.listener = listener;
        this.repository = repository;
    }

    public void getHomeWork(IReposytoryHomeWork repo, String cookie){


        if (mustTasks==null)
            repo.getHomeWorkFromInternet(cookie);
        else listener.setTests(mustTasks,index);

    }

    @Override
    public void recieveHomeWork(List<HomeWorks> homeWorks) {


         mustTasks = new ArrayList<>();

        for (HomeWorks homeWork: homeWorks){

            for (Tasks tasks: homeWork.tasks) {

                    if (tasks.task.task_type.equals(TASK_TYPE)) {

                        mustTasks.add(new MustTask(homeWork.title, tasks.task));
                    }


            }
        }

        mustTasks.sort((o1, o2) -> {

            if (o1.task.contest_info.contest_status.status.equals(o2.task.contest_info.contest_status.status)) return 0;

            else if (o1.task.contest_info.contest_status.status.equals(CONTEST_STATUS_FIRST) &&
            o2.task.contest_info.contest_status.status.equals(CONTEST_STATUS_SECOND)) return 1;

            else if (o1.task.contest_info.contest_status.status.equals(CONTEST_STATUS_SECOND) &&
                    o2.task.contest_info.contest_status.status.equals(CONTEST_STATUS_THIRD)) return 1;

            else if (o1.task.contest_info.contest_status.status.equals(CONTEST_STATUS_FIRST) &&
                    o2.task.contest_info.contest_status.status.equals(CONTEST_STATUS_THIRD)) return 1;

            else return -1;
        });

        int schetchik = 0;
        index = -1;
        for (MustTask mustTask: mustTasks){
            if (mustTask.task.contest_info.contest_status.status.equals(CONTEST_STATUS_SECOND))
                index = schetchik;
            schetchik++;
        }


        listener.setTests(mustTasks,index);

    }


    @Override
    public void startTest(String cookie, String testId) {

        this.cookie = cookie;
        this.testId = testId;

        repository.initRetrofit()
                .startTest(RegistrationResponse.cookie, referer, RegistrationResponse.token, testId, Pust.INSTANCE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response->{
                    if (response.equals("true") || response.equals("already started")){
                        listener.startTest();
                    }
                    else {
                        listener.onError();
                    }
                }, error->{
                    listener.onError();
                });
    }

    @Override
    public void onNoInternet(Throwable error) {

    }
}
