package com.alexander.finalproject.mainscreen.homework.test.testdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Contest {

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("time_left")
    @Expose
    public int timeLeft;

    @SerializedName("duration")
    @Expose
    public int duration;

}
