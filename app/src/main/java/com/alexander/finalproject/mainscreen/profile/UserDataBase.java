package com.alexander.finalproject.mainscreen.profile;

import android.arch.persistence.room.RoomDatabase;
import com.alexander.finalproject.mainscreen.profile.data.UserDao;

@android.arch.persistence.room.Database(entities = {com.alexander.finalproject.mainscreen.profile.data.User.class}, version = 1)
public abstract class UserDataBase extends RoomDatabase {
    public abstract UserDao userDao();
}
