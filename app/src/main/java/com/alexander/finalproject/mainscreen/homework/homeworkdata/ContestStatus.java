package com.alexander.finalproject.mainscreen.homework.homeworkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContestStatus {

    @SerializedName("status")
    @Expose
    public String status;
}
