package com.alexander.finalproject.mainscreen.homework.test;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import android.util.Log;
import android.view.View;
import android.widget.*;
import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.R;
import com.alexander.finalproject.Repository;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.homework.test.testdata.Questions;

import java.util.ArrayList;
import java.util.List;


public class TestActivity extends AppCompatActivity implements TestContract.TestActivityInterface {

    private static final String TEST_TITLE = "test_title";
    private static final String TEST_ID = "test_id";
    private static final String MULTIPLE = "SELECT_MULTIPLE";

    private int currentQuestion=0;

    TestFragment fragment;
    TestContract.TestPresenterInteface presenter;
    IRepository repository;

    List<TestFragment> fragments;

    private TextView textView;
    private TextView nomer;

    private ProgressBar progressBar;

    private LinearLayout linearLayout;

    private ImageButton buttonContinue;
    private ImageButton buttonNext;
    private ImageButton buttonEnd;

    private String testId;

    public static void start(Activity activity, String testTitle, String testId) {
        Intent intent = new Intent(activity, TestActivity.class);
        intent.putExtra(TEST_TITLE, testTitle);
        intent.putExtra(TEST_ID, testId);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_acrivity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String title = (String) getIntent().getSerializableExtra(TEST_TITLE);
        testId = (String) getIntent().getSerializableExtra(TEST_ID);

        if (title!=null && toolbar!=null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        repository = Repository.getInstance();
        presenter = new TestPresenter(this, repository);

        //получение вопросов
        ((TestPresenter) presenter).getQuestions(RegistrationResponse.cookie,testId);


        toolbar.setNavigationOnClickListener(v ->  onBackPressed());

        buttonContinue = findViewById(R.id.continueTest);
        buttonNext = findViewById(R.id.next);
        buttonEnd = findViewById(R.id.end);

        nomer = findViewById(R.id.nomerVopr);

        progressBar = findViewById(R.id.load);


        buttonEnd.setOnClickListener(v->{
            presenter.sendAnswer(fragments.get(currentQuestion).answer, currentQuestion+1, testId);
            finish();
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }

    @Override
    public void showTest(List<Questions> questions) {

        linearLayout = findViewById(R.id.head);


        nomer.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        for (int i= 0; i<questions.size(); i++) {

            textView = new TextView(this);
            textView.setText(String.valueOf(i+1));
            textView.setTextSize(20);
            textView.setPadding(30,10,30,10);
            if (questions.get(i).problemStatus.equals("AC"))
                textView.setTextColor(getResources().getColor(R.color.green,null));
            else textView.setTextColor(getResources().getColor(R.color.red,null));
            textView.setOnClickListener(v -> {
                currentQuestion = Integer.parseInt(((TextView) v).getText().toString())-1;
                getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container,fragments.get(currentQuestion))
                    .addToBackStack("")
                    .commit(); });

            linearLayout.addView(textView);
        }

        fragments = new ArrayList<>();

        for (Questions question: questions){
            ArrayList<String> listQuestions = new ArrayList<>();
            String str = question.problem.page.titleQuestion;
            String x = str.substring(str.indexOf('>')+1,str.lastIndexOf('<'));
            listQuestions.add(x);
            listQuestions.addAll(question.problem.answerChoises);


            if (question.problem.problemType.equals(MULTIPLE))
                fragment = TestFragment.newInstance(listQuestions,true);
            else
                fragment = TestFragment.newInstance(listQuestions,false);

           fragments.add(fragment);
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container,fragments.get(0))
                .addToBackStack("")
                .commit();


        buttonNext.setVisibility(View.VISIBLE);
        buttonEnd.setVisibility(View.VISIBLE);
        buttonContinue.setVisibility(View.VISIBLE);

        buttonNext.setClickable(true);
        buttonEnd.setClickable(true);
        buttonContinue.setClickable(true);


        buttonContinue.setOnClickListener(v->next());
        buttonNext.setOnClickListener(v->{
            presenter.sendAnswer(fragments.get(currentQuestion).answer, currentQuestion+1, testId);
            next();
        });

    }

    @Override
    public void onError() {

        progressBar.setVisibility(View.GONE);
        Toast.makeText(this,R.string.errorChange,Toast.LENGTH_SHORT).show();

    }

    @Override
    public void refresh(int id) {
        TextView text = (TextView) linearLayout.getChildAt(id);
        text.setTextColor(getResources().getColor(R.color.green,null));
    }

    public void next(){
        if (currentQuestion==fragments.size()-1)
            currentQuestion=0;
        else currentQuestion++;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container,fragments.get(currentQuestion))
                .addToBackStack("")
                .commit();
    }

    public void back(){
        if (currentQuestion==0)
            finish();
        else currentQuestion--;
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container,fragments.get(currentQuestion))
                .addToBackStack("")
                .commit();
    }
}


