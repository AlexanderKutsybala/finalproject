package com.alexander.finalproject.mainscreen.profile.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import com.alexander.finalproject.Convert;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Entity
public class User {

    @PrimaryKey
    private int ud;

    @SerializedName("birthday")
    @Expose
    private String birthday;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("first_name")
    @Expose
    private String name;

    @SerializedName("last_name")
    @Expose
    private String lastName;

    @SerializedName("middle_name")
    @Expose
    private String middleName;

    @SerializedName("phone_mobile")
    @Expose
    private String phone;

    @SerializedName("t_shirt_size")
    @Expose
    private String size;

    @SerializedName("is_client")
    @Expose
    private boolean client;

    @SerializedName("skype_login")
    @Expose
    private String skype;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("school")
    @Expose
    private String school;

    @SerializedName("school_graduation")
    @Expose
    private String graduate;

    @SerializedName("faculty")
    @Expose
    private String faculty;

    @SerializedName("university_graduation")
    @Expose
    private long graduateUn;

    @SerializedName("grade")
    @Expose
    private String grade;

    @SerializedName("department")
    @Expose
    private String department;

    @SerializedName("current_work")
    @Expose
    private String work;

    @SerializedName("resume")
    @Expose
    private String resume;

    @TypeConverters({Convert.class})
    @SerializedName("notifications")
    @Expose
    private List<String> notifications;

    @SerializedName("id")
    @Expose
    private long idE;

    @SerializedName("admin")
    @Expose
    private boolean admin;

    @SerializedName("region")
    @Expose
    private String region;

    @SerializedName("university")
    @Expose
    private String university;

    @SerializedName("avatar")
    @Expose
    private String avatar;




    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getUd() {
        return ud;
    }

    public void setUd(int ud) {
        this.ud = ud;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public boolean getClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getGraduate() {
        return graduate;
    }

    public void setGraduate(String graduate) {
        this.graduate = graduate;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public long getGraduateUn() {
        return graduateUn;
    }

    public void setGraduateUn(long graduateUn) {
        this.graduateUn = graduateUn;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public List<String> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<String> notifications) {
        this.notifications = notifications;
    }

    public long getIdE() {
        return idE;
    }

    public void setIdE(long idE) {
        this.idE = idE;
    }

    public boolean getAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
}
