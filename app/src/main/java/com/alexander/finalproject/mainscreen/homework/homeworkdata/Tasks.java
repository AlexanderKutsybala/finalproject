package com.alexander.finalproject.mainscreen.homework.homeworkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tasks {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("task")
    @Expose
    public Task task;
}
