package com.alexander.finalproject.mainscreen.profile;

import android.util.Log;
import com.alexander.finalproject.AppDataBase;
import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.profile.data.ChangUser;
import com.alexander.finalproject.mainscreen.profile.data.MainUser;
import com.alexander.finalproject.mainscreen.profile.data.User;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.function.LongFunction;


public class ProfilePresenter implements ProfileContract.IProfilePresenter{


    private final String referer = "https://fintech.tinkoff.ru";
    ProfileContract.IProfileView listener;
    UserDataBase database;
    int id;

    public ProfilePresenter(ProfileContract.IProfileView listener){
        this.listener = listener;
    }


    @Override
    public void getProfile(IRepository repo, String cookie) {

        listener.showLoading();

        database = AppDataBase.getInstance().getDatabase();

        repo.initRetrofit()
                .getProfile(cookie)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Single.just(response)
                            .subscribeOn(Schedulers.io())
                            .subscribe(v->{
                                User dataBaseUser = new User();
                                dataBaseUser.setUd(0);
                                dataBaseUser.setIdE(response.getUser().getIdE());
                                dataBaseUser.setAdmin(response.getUser().getAdmin());
                                dataBaseUser.setClient(response.getUser().getClient());
                                dataBaseUser.setDepartment(response.getUser().getDepartment());
                                dataBaseUser.setDescription(response.getUser().getDescription());
                                dataBaseUser.setFaculty(response.getUser().getFaculty());
                                dataBaseUser.setGrade(response.getUser().getGrade());
                                dataBaseUser.setGraduate(response.getUser().getGraduate());
                                dataBaseUser.setWork(response.getUser().getWork());
                                dataBaseUser.setUd(response.getUser().getUd());
                                dataBaseUser.setSkype(response.getUser().getSkype());
                                dataBaseUser.setSize(response.getUser().getSize());
                                dataBaseUser.setSchool(response.getUser().getSchool());
                                dataBaseUser.setResume(response.getUser().getResume());
                                dataBaseUser.setPhone(response.getUser().getPhone());
                                dataBaseUser.setNotifications(response.getUser().getNotifications());
                                dataBaseUser.setGraduateUn(response.getUser().getGraduateUn());
                                dataBaseUser.setName(response.getUser().getName());
                                dataBaseUser.setLastName(response.getUser().getLastName());
                                dataBaseUser.setMiddleName(response.getUser().getMiddleName());
                                dataBaseUser.setAvatar(response.getUser().getAvatar());
                                dataBaseUser.setRegion(response.getUser().getRegion());
                                dataBaseUser.setUniversity(response.getUser().getUniversity());
                                dataBaseUser.setEmail(response.getUser().getEmail());
                                dataBaseUser.setBirthday(response.getUser().getBirthday());

                                try{
                                    database.userDao().insert(dataBaseUser);}
                                catch (Exception e){
                                    database.userDao().update(dataBaseUser);
                                }
                            });

                    listener.showProfile(response);
                }, error -> listener.showError());
    }


    @Override
    public void changeProfile(IRepository repo, String cookie, MainUser mainUser) {
        listener.showLoading();


        ChangUser user= new ChangUser();
        user.setBirthday(mainUser.getUser().getBirthday());
        user.setEmail(mainUser.getUser().getEmail());
        user.setName(mainUser.getUser().getName());
        user.setLastName(mainUser.getUser().getLastName());
        user.setMiddleName(mainUser.getUser().getMiddleName());

        database = AppDataBase.getInstance().getDatabase();

        database.userDao().getById(0).subscribeOn(Schedulers.io()).subscribe(v->{
            user.setPhone(v.getPhone());
            user.setSize(v.getSize());
            user.setClient(v.getClient());
            user.setSkype(v.getSkype());
            user.setDescription(v.getDescription());

        user.setRegion(mainUser.getUser().getRegion());

            user.setSchool(v.getSchool());
            user.setGraduate(v.getGraduate());

        user.setUniversity(mainUser.getUser().getUniversity());


            user.setFaculty(v.getFaculty());
            user.setGraduateUn(v.getGraduateUn());
            user.setGraduate(v.getGraduate());
            user.setGrade(v.getGrade());
            user.setDepartment(v.getDepartment());
            user.setWork(v.getWork());
            user.setResume(v.getResume());
            user.setNotifications(v.getNotifications());
            user.setIdE(v.getIdE());
            user.setAdmin(v.getAdmin());
            user.setAvatar(mainUser.getUser().getAvatar());});





        repo.initRetrofit()
                .changeProfile(cookie, referer, RegistrationResponse.token, user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response->{

                    Single.just(response)
                            .subscribeOn(Schedulers.io())
                            .subscribe(v->{
                    User dataBaseUser = new User();
                    dataBaseUser.setUd(0);
                    dataBaseUser.setName(mainUser.getUser().getName());
                    dataBaseUser.setIdE(mainUser.getUser().getIdE());
                    dataBaseUser.setAdmin(mainUser.getUser().getAdmin());
                    dataBaseUser.setClient(mainUser.getUser().getClient());
                    dataBaseUser.setDepartment(mainUser.getUser().getDepartment());
                    dataBaseUser.setDescription(mainUser.getUser().getDescription());
                    dataBaseUser.setFaculty(mainUser.getUser().getFaculty());
                    dataBaseUser.setGrade(mainUser.getUser().getGrade());
                    dataBaseUser.setGraduate(mainUser.getUser().getGraduate());
                    dataBaseUser.setWork(mainUser.getUser().getWork());
                    dataBaseUser.setUd(mainUser.getUser().getUd());
                    dataBaseUser.setSkype(mainUser.getUser().getSkype());
                    dataBaseUser.setSize(mainUser.getUser().getSize());
                    dataBaseUser.setSchool(mainUser.getUser().getSchool());
                    dataBaseUser.setResume(mainUser.getUser().getResume());
                    dataBaseUser.setPhone(mainUser.getUser().getPhone());
                    dataBaseUser.setNotifications(mainUser.getUser().getNotifications());
                    dataBaseUser.setGraduateUn(mainUser.getUser().getGraduateUn());
                    dataBaseUser.setLastName(mainUser.getUser().getLastName());
                    dataBaseUser.setMiddleName(mainUser.getUser().getMiddleName());
                    dataBaseUser.setAvatar(mainUser.getUser().getAvatar());
                    dataBaseUser.setRegion(mainUser.getUser().getRegion());
                    dataBaseUser.setUniversity(mainUser.getUser().getUniversity());
                    dataBaseUser.setEmail(mainUser.getUser().getEmail());
                    dataBaseUser.setBirthday(mainUser.getUser().getBirthday());
                    database.userDao().update(dataBaseUser);
                            });

                    listener.showMessage();},error->listener.showError());
    }

    @Override
    public void showProfile(IRepository repository) {

        listener.showLoading();
        database=AppDataBase.getInstance().getDatabase();
        database.userDao().getById(0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableSingleObserver<User>() {
                    @Override
                    public void onSuccess(User user) {
                        MainUser mainUser = new MainUser();
                        mainUser.setUser(user);
                        listener.showProfile(mainUser);
                    }

                    @Override
                    public void onError(Throwable e) {
                        getProfile(repository,RegistrationResponse.cookie);
                    }
                });
    }


}
