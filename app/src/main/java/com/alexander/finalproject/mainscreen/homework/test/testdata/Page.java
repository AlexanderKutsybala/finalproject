package com.alexander.finalproject.mainscreen.homework.test.testdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Page {
    @SerializedName("unstyled_statement")
    @Expose
    public String titleQuestion;
}
