package com.alexander.finalproject.mainscreen.homework.test.testdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Problem {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("cms_page")
    @Expose
    public Page page;

    @SerializedName("problem_type")
    @Expose
    public String problemType;

    @SerializedName("answer_choices")
    @Expose
    public List<String> answerChoises;


}
