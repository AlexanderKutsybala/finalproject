package com.alexander.finalproject.mainscreen.profile;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.alexander.finalproject.R;
import com.alexander.finalproject.Repository;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.profile.data.MainUser;
import com.alexander.finalproject.mainscreen.profile.data.User;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

public class Profile extends Fragment implements ProfileContract.IProfileView{

    public static final String ARG_PAGE = "ARG_PAGE";

    private String logoUrl;

    private ImageView avatar;

    private EditText birthday;
    private EditText email;
    private EditText name;
    private EditText lastName;
    private EditText middleName;
    private EditText region;
    private EditText university;

    private android.support.v7.widget.Toolbar toolbar;

    private Button buttonChange;
    private FloatingActionButton buttonRefresh;

    private ProgressBar progressBar;

    private int mPage;

    private ProfileContract.IProfilePresenter presenter;

    public Profile(){

    }

    public static Profile newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        Profile fragment = new Profile();
        fragment.setArguments(args);
        return fragment;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPage = getArguments().getInt(ARG_PAGE);
        }


    }

    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                       Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile, container, false);

        avatar = view.findViewById(R.id.avatar);
        birthday = view.findViewById(R.id.birthday);
        email = view.findViewById(R.id.email);
        name = view.findViewById(R.id.name);
        lastName = view.findViewById(R.id.lastName);
        middleName = view.findViewById(R.id.middleName);
        region = view.findViewById(R.id.region);
        university = view.findViewById(R.id.university);

        toolbar = view.findViewById(R.id.toolbar);

        buttonChange=view.findViewById(R.id.buttonChange);
        buttonRefresh=view.findViewById(R.id.refresh);

        progressBar=view.findViewById(R.id.loadingBar);

        presenter = new ProfilePresenter(this);

        presenter.showProfile(Repository.getInstance());


        Glide.with(this)

                .load(logoUrl)
                .placeholder(R.drawable.group_2)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(avatar);

        return view;

    }

    @Override
    public void showProfile(MainUser mainUser) {

        toolbar.setTitle(mainUser.getUser().getName()+" "+ mainUser.getUser().getLastName());

        birthday.setText(mainUser.getUser().getBirthday());
        email.setText(mainUser.getUser().getEmail());
        name.setText(mainUser.getUser().getName());
        lastName.setText(mainUser.getUser().getLastName());
        middleName.setText(mainUser.getUser().getMiddleName());
        region.setText(mainUser.getUser().getRegion());
        university.setText(mainUser.getUser().getUniversity());

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                buttonChange.setEnabled(true);

            }
        };

        birthday.addTextChangedListener(textWatcher);
        email.addTextChangedListener(textWatcher);
        name.addTextChangedListener(textWatcher);
        lastName.addTextChangedListener(textWatcher);
        middleName.addTextChangedListener(textWatcher);
        region.addTextChangedListener(textWatcher);
        university.addTextChangedListener(textWatcher);

        logoUrl = "https://fintech.tinkoff.ru"+mainUser.getUser().getAvatar();

        Glide.with(this)
                .load(logoUrl)
                .placeholder(R.drawable.group_2)
                .into(avatar);

        progressBar.setVisibility(View.INVISIBLE);

        buttonChange.setEnabled(false);
        buttonChange.setOnClickListener(v->{
            mainUser.setUser(readUserWrite());
            presenter.changeProfile(Repository.getInstance(),RegistrationResponse.cookie, mainUser);});
        buttonRefresh.setOnClickListener(v->presenter.getProfile(Repository.getInstance(),RegistrationResponse.cookie));

    }

    @Override
    public void showLoading() {

        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {


        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(getContext(),getResources().getText(R.string.errorChange),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage() {

        progressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(getContext(),getResources().getText(R.string.trueChange),Toast.LENGTH_SHORT).show();
    }

    public User readUserWrite(){

        User user = new User();
        user.setBirthday(birthday.getText().toString());
        user.setEmail(email.getText().toString());
        user.setName(name.getText().toString());
        user.setLastName(lastName.getText().toString());
        user.setMiddleName(middleName.getText().toString());
        user.setRegion(region.getText().toString());
        user.setUniversity(university.getText().toString());
        return user;

    }
}
