package com.alexander.finalproject.mainscreen.homework.homeworkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContestInfo {

    @SerializedName("contest_status")
    @Expose
    public ContestStatus contest_status;

    @SerializedName("contest_url")
    @Expose
    public String contest_url;
}
