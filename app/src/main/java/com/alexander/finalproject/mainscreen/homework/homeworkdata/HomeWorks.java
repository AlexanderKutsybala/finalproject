package com.alexander.finalproject.mainscreen.homework.homeworkdata;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HomeWorks {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("tasks")
    @Expose
    public ArrayList<Tasks> tasks;
}
