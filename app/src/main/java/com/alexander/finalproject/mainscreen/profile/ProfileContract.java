package com.alexander.finalproject.mainscreen.profile;

import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.mainscreen.profile.data.MainUser;

import java.util.List;

public interface ProfileContract {

    interface IProfileView{

        void showProfile(MainUser mainUser);
        void showLoading();
        void showError();
        void showMessage();
    }

    interface IProfilePresenter{

        void getProfile(IRepository repo, String cookie);
        void changeProfile(IRepository repo, String cookie, MainUser mainUser);
        void showProfile(IRepository repository);
    }
}
