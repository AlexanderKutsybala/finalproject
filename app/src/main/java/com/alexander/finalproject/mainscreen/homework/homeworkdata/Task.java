package com.alexander.finalproject.mainscreen.homework.homeworkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Task {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("title")
    @Expose
    public String title;

    @SerializedName("task_type")
    @Expose
    public String task_type;

    @SerializedName("contest_info")
    @Expose
    public ContestInfo contest_info;

    @SerializedName("short_name")
    @Expose
    public String short_name;

    public String toString(){
        if (title!=null && task_type!=null && short_name!=null)
            return id+title+task_type+short_name;
        else return "фигня";
    }
}
