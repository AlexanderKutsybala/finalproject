package com.alexander.finalproject.mainscreen.homework.test.testdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TestStatus {

    @SerializedName("contest")
    @Expose
    public Contest contest;

    @SerializedName("problems")
    @Expose
    public List<String> problems;
}
