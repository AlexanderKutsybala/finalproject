package com.alexander.finalproject.mainscreen.homework;

import com.alexander.finalproject.Api;
import com.alexander.finalproject.mainscreen.IReposytoryHomeWork;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.List;


public class ReposytoryHomeWork implements IReposytoryHomeWork {

    Api api;
    HomeWorkContract.HomeWorkPresenter listener;


    public ReposytoryHomeWork(HomeWorkContract.HomeWorkPresenter listener){

        this.listener = listener;
    }

    public void initRetrofit(){

        api = new Retrofit.Builder()
                .baseUrl("https://fintech.tinkoff.ru")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Api.class);
    }

    @Override
    public void getHomeWorkFromInternet(String cookie){
        initRetrofit();

        api.getHomeWorks(cookie)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> listener.recieveHomeWork(response.homeworks),
                        error -> listener.onNoInternet(error));

    }

}
