package com.alexander.finalproject.mainscreen.homework;

import com.alexander.finalproject.mainscreen.homework.homeworkdata.HomeWorks;
import com.alexander.finalproject.mainscreen.homework.homeworkdata.MustTask;

import java.util.List;

public interface HomeWorkContract {

    interface MyCourseFragment{

        void setTests(List<MustTask> mustTusks, int index);
        void startTest();
        void onError();
    }

    interface HomeWorkPresenter{

        void recieveHomeWork(List<HomeWorks> homeWorks);
        void startTest(String cookie, String testId);

        void onNoInternet(Throwable error);
    }
}
