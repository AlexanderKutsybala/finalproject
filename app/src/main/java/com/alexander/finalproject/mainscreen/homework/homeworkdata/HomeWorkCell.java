package com.alexander.finalproject.mainscreen.homework.homeworkdata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HomeWorkCell {

    @SerializedName("homeworks")
    @Expose
    public ArrayList<HomeWorks> homeworks;
}
