package com.alexander.finalproject.mainscreen.homework.test;

import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.mainscreen.homework.test.testdata.Answer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;



public class TestPresenter implements TestContract.TestPresenterInteface {

    private IRepository repositoryTest;
    private TestContract.TestActivityInterface listener;
    private String testId;
    private final String referer = "https://fintech.tinkoff.ru";

    public TestPresenter(TestContract.TestActivityInterface listener, IRepository repository){

        this.listener = listener;
        this.repositoryTest = repository;
    }

    public void getQuestions(String cookie, String testId){

        repositoryTest.initRetrofit()
                .getQuestions(cookie,testId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response->listener.showTest(response), error->listener.onError());
    }

    public void sendAnswer(int [] answers, int id, String testId){

        this.testId = testId;
        Answer answer = new Answer();
        answer.answer="";

        for (int i=1;i<answers.length;i++){
            answer.answer+=answers[i];
        }


        repositoryTest.initRetrofit()
                .answer(RegistrationResponse.cookie, referer, RegistrationResponse.token, testId, String.valueOf(id), answer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response->chekTestStatus(id), error->listener.onError());
    }

    public void chekTestStatus(int id){

        repositoryTest.initRetrofit()
                .chekTestStatus(RegistrationResponse.cookie, testId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response->listener.refresh(id-1),error -> listener.onError());
    }
}
