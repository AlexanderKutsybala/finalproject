package com.alexander.finalproject;

import android.app.Application;
import android.arch.persistence.room.Room;
import com.alexander.finalproject.mainscreen.profile.UserDataBase;

public class AppDataBase extends Application {

    public static AppDataBase instance;

    private UserDataBase database;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, UserDataBase.class, "database")
                .build();
    }

    public static AppDataBase getInstance() {
        return instance;
    }

    public UserDataBase getDatabase() {
        return database;
    }
}
