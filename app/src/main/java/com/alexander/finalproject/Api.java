package com.alexander.finalproject;

import com.alexander.finalproject.login.data.RegistrationBody;
import com.alexander.finalproject.login.data.RegistrationResponse;
import com.alexander.finalproject.login.data.Status;
import com.alexander.finalproject.mainscreen.homework.homeworkdata.HomeWorkCell;
import com.alexander.finalproject.mainscreen.homework.test.testdata.*;
import com.alexander.finalproject.mainscreen.profile.data.ChangUser;
import com.alexander.finalproject.mainscreen.profile.data.MainUser;
import com.alexander.finalproject.mainscreen.profile.data.User;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.*;

import java.util.List;


public interface Api {

    @POST("/api/signin")
    Single<Response<RegistrationResponse>> registerUser(@Body RegistrationBody registrationBody);

    @GET("/api/user")
    Single<MainUser> isRegisterUser(@Header("Cookie") String userCookie);

    @GET("/api/course/android_fall2018/homeworks")
    Single<HomeWorkCell> getHomeWorks(@Header("Cookie") String userCookie);

    @POST("/api/contest/{id}/start_contest")
    Single<String> startTest(@Header("Cookie") String userCookie, @Header("Referer") String referer, @Header("X-CSRFToken") String token, @Path("id") String id, @Body Pust pustota);

    @GET("/api/contest/{id}/status")
    Single<TestStatus> chekTestStatus(@Header("Cookie") String userCookie, @Path("id") String id);

    @GET("/api/contest/{id}/problems")
    Single<List<Questions>> getQuestions(@Header("Cookie") String userCookie, @Path("id") String id);

    @GET("/api/contest/{id}/problems/{testId}")
    Single<Questions> getOneQuestion(@Header("Cookie") String userCookie, @Path("id") String id, @Path("testId") String testId);

    @POST("/api/contest/{id}/problem/{testId}")
    Single<Response<Status>> answer(@Header("Cookie") String userCookie, @Header("Referer") String referer,  @Header("X-CSRFToken") String token, @Path("id") String id, @Path("testId") String testId, @Body Answer answer);

    @GET("/api/user")
    Single<MainUser> getProfile(@Header("Cookie") String userCookie);

    @PUT("/api/register_user")
    Single<Response<String>> changeProfile(@Header("Cookie") String userCookie, @Header("Referer") String referer,  @Header("X-CSRFToken") String token, @Body ChangUser user);

    @POST("/api/signout")
    Single<okhttp3.ResponseBody> signOut(@Header("Cookie") String userCookie, @Header("Referer") String referer, @Header("X-CSRFToken") String token, @Body Pust pustota);


}



