package com.alexander.finalproject.login;

import java.util.List;

public interface LoginContract {

    interface ILoginActivity{

        void onErrorLogin();

        void onNoInternet();

        void onOkLogin(List<String> cookie);
    }

    interface ILoginPresenter{

        void onOkReg(List<String> cookie);

        void onErrorReg(Throwable error);
    }

}
