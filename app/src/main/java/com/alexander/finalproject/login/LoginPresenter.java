package com.alexander.finalproject.login;

import com.alexander.finalproject.IRepository;
import com.alexander.finalproject.login.data.RegistrationBody;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.HttpException;

import java.util.List;


public class LoginPresenter implements LoginContract.ILoginPresenter {


    private LoginContract.ILoginActivity listener;

    public LoginPresenter(LoginContract.ILoginActivity listener){
        this.listener = listener;
    }


    @Override
    public void onErrorReg(Throwable error) {

        if (error instanceof HttpException)
            listener.onErrorLogin();
        else listener.onNoInternet();

    }

    @Override
    public void onOkReg(List<String> cookie) {

        if (cookie.size()==1){
            listener.onErrorLogin();
        }
        else if (cookie!=null)
        listener.onOkLogin(cookie);
        else listener.onErrorLogin();
    }


    public void register(String email, String password, IRepository repository) {

        RegistrationBody registrationBody = new RegistrationBody();
        registrationBody.email=email;
        registrationBody.password=password;

        repository.initRetrofit()
                .registerUser(registrationBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response->{
                    onOkReg(response.headers().values("Set-Cookie"));

                }, error->onErrorReg(error));

    }
}
