package com.alexander.finalproject.login

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.alexander.finalproject.R
import com.alexander.finalproject.Repository
import com.alexander.finalproject.login.data.RegistrationResponse
import com.alexander.finalproject.mainscreen.MainScreenActivity


class LoginActivity : AppCompatActivity(){


    private var myPreferences = "myPrefs"


    lateinit var signInButton: Button
    lateinit var login: EditText
    lateinit var password: EditText
    lateinit var loading: ProgressBar

    lateinit var sharedPreferences: SharedPreferences

      val listener = object: LoginContract.ILoginActivity{


          override fun onOkLogin(cookie: List<String>) {
              val editor = sharedPreferences!!.edit()

              val cookies = ArrayList<String>()
              cookies.addAll(cookie)

              editor.putString("cookie0",cookies.get(0))
              editor.putString("cookie1",cookies.get(1))
              editor.putString("cookie2",cookies.get(2))

              editor.apply()



              RegistrationResponse.cookie = (sharedPreferences.getString("cookie2","0"))+"; "+
              (sharedPreferences.getString("cookie1","0"))


              val str = sharedPreferences.getString("cookie1", "0").substringBefore(";")
              RegistrationResponse.token=str.substringAfter("=")

              uiEnable(signInButton, login, password, loading)


              startActivity(Intent(this@LoginActivity, MainScreenActivity::class.java))
              finish()
          }

        override fun onErrorLogin() {

            Toast.makeText(this@LoginActivity, R.string.no_wright_login, Toast.LENGTH_SHORT).show()
            uiEnable(signInButton, login, password, loading)

        }

        override fun onNoInternet() {

            Toast.makeText(this@LoginActivity, R.string.no_internet, Toast.LENGTH_SHORT).show()
            uiEnable(signInButton, login, password, loading)
        }

      }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        sharedPreferences = getSharedPreferences(myPreferences, Context.MODE_PRIVATE)

        login = findViewById(R.id.login)
        password = findViewById(R.id.password)
        signInButton = findViewById(R.id.signIn)
        loading = findViewById(R.id.loadingBar)



        val str = sharedPreferences.getString("cookie1", "0").substringBefore(";")
        RegistrationResponse.token=str.substringAfter("=")

        RegistrationResponse.cookie=(sharedPreferences.getString("cookie2","0"))+"; "+
        (sharedPreferences.getString("cookie1","0"))


        val myPresenter = LoginPresenter(listener)
        val repository = Repository.getInstance()



        signInButton.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {

                signInButton.isClickable=false
                loading.visibility=View.VISIBLE
                login.isEnabled=false
                password.isEnabled=false
                myPresenter.register(login.text.toString(), password.text.toString(), repository)
            }

        })
    }


    fun uiEnable(button: Button, login: EditText, password: EditText, loading: View){

        button.isClickable=true
        loading.visibility=View.GONE
        login.isEnabled=true
        password.isEnabled=true
    }
}
