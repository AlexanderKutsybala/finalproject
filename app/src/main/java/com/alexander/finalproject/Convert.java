package com.alexander.finalproject;

import android.arch.persistence.room.TypeConverter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Convert {

    @TypeConverter
    public String fromNotif(List<String> notifications) {
        if (notifications!=null)
        return notifications.stream().collect(Collectors.joining(","));
        else return "";
    }

    @TypeConverter
    public List<String> toNotif(String data) {
        return Arrays.asList(data.split(","));
    }

}
